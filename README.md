# Hi, i'm max.

😺 I like cats and programming

🐧 I use [Arch Linux](https://archlinux.org) with [Hyprland](https://hyprland.org)

🖥️ PC: `Intel Core i5-10400F`, `Radeon RX 6700 XT`

⌨️ Keyboard: NuPhy Halo75 (Rose Glacier) with the [Colemak](https://colemak.com/) layout

💻 Laptop: `Thinkpad T480s`, `Intel Core i5-8250U`

💾 I'm familiar with: 
- 🐍 Python
- ☕ Java
- 🎮 C#/Unity
- 📦 NodeJS
- 🌐 HTML/CSS 
- 🔧 C++
- 👑 Nim
- and some other stuff...

💡 I want to learn Godot, Rust, and Svelte

🔑 GPG: `0EE73B64131E4E68` `Max Cooley (Maxb0tbeep) <max@polygonal.place>`

🪙 Send me Monero: [84epa...3hNtc](monero:84epa9iY6WS8EzYEE5cbNb3mr9G8zQwv53w4vuptJkhTWYQ7ZXrNPHoFoNPdwmp6PGNKVnsiEnGQU2BvdXSYVYpB6d3hNtc)
